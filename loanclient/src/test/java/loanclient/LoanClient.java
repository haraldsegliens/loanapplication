package loanclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.hegliens.loanclient.models.LoanModel;

import com.google.gson.Gson;

public class LoanClient {

	public static void main(String[] args) throws IOException {
		
		Gson gson = new Gson();
		URL obj = new URL("http://localhost:8080/loanapi/webapi/loans");
		
        for(int i = 0; i < 1; i++)
        {
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	        con.setRequestMethod("POST");
	        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	 
	        // For POST only - START
	        LoanModel model = new LoanModel();
	        model.setID(i);
	        model.setLoanAmount(100.0f);
	        model.setName("K�rlis");
	        model.setSurname("Daudz");
	        model.setPersId("050489-10190");
	        model.setTerm("25-12-2016 23:22:22");
	        con.setDoOutput(true);
	        OutputStream os = con.getOutputStream();
	        os.write(gson.toJson(model).getBytes("UTF-8"));
	        os.flush();
	        os.close();
	        // For POST only - END
	 
	        int responseCode = con.getResponseCode();
	        System.out.println("POST Response Code :: " + responseCode);
	 
	        BufferedReader in = new BufferedReader(new InputStreamReader( con.getErrorStream() == null ? con.getInputStream() : con.getErrorStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
 
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            System.out.println(response.toString());
        }
	}

}
