package org.hegliens.loanclient.models;

import org.hegliens.loanclient.utils.DateParam;
import javax.xml.bind.annotation.*;
 
@XmlRootElement
public class LoanModel {
	
	public LoanModel(){}
	private int ID;
	private float loanAmount;
	private DateParam term;
	private String name;
	private String surname;
	private String persId;
	private String originCountry;
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public float getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(float loanAmount) {
		this.loanAmount = loanAmount;
	}
	public String getTerm() {
		return term.toString();
	}
	public void setTerm(String term) {
		this.term = new DateParam(term);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPersId() {
		return persId;
	}
	public void setPersId(String persId) {
		this.persId = persId;
	}
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
}
