package org.hegliens.loanclient.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

//yyyy.MM.dd HH:mm:ss UTC
public class DateParam {
	  private Date date;
	  public DateParam(){date = null;}
	  public DateParam(String dateStr) {
		    if (dateStr == "") {
		    	this.date = null;
		    	return;
		    }
		    final DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		    try {
		    	this.date = dateFormat.parse(dateStr);
		    } catch (Exception e) {
		    }
		    this.date = null;
	  }
	  
	  public String toString()
	  {
		  	if(date == null) return "";
		  	final DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		    return dateFormat.format(date);
	  }

	  public Date getDate() {
	    return date;
	  }
	}
