package org.hegliens.loanapi.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BlacklistModel {
	private int ID;
	private String persId;
	
	public BlacklistModel(){}
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getPersId() {
		return persId;
	}
	public void setPersId(String persId) {
		this.persId = persId;
	}
}
