package org.hegliens.loanapi.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProcessLoanResponseModel {
	private String response;
	private String ipAddress;
	private LoanModel loan;
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public LoanModel getLoan() {
		return loan;
	}
	public void setLoan(LoanModel loan) {
		this.loan = loan;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
}
