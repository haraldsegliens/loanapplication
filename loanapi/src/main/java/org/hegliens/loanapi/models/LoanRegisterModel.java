package org.hegliens.loanapi.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoanRegisterModel {
	private String CountryCode;
	private int CreatedTimeSeconds;
	private int Calls;
	public int getCreatedTime() {
		return CreatedTimeSeconds;
	}
	public void setCreatedTime(int createdTime) {
		CreatedTimeSeconds = createdTime;
	}
	public int getCalls() {
		return Calls;
	}
	public void setCalls(int calls) {
		Calls = calls;
	}
	public String getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	
}
