package org.hegliens.loanapi.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

//yyyy.MM.dd HH:mm:ss UTC
public class DateParam {
	  private final Date date;
	  public DateParam(){date = null;}
	  public DateParam(String dateStr) throws WebApplicationException {
		    if (dateStr == "") {
		    	this.date = null;
		    	return;
		    }
		    final DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		    try {
		    	this.date = dateFormat.parse(dateStr);
		    } catch (ParseException e) {
			      throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
			        .entity("Couldn't parse date string: " + e.getMessage())
			        .build());
		    }
	  }
	  
	  public String toString()
	  {
		  	if(date == null) return "";
		  	final DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		    return dateFormat.format(date);
	  }

	  public Date getDate() {
	    return date;
	  }
	}
