package org.hegliens.loanapi.resources;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hegliens.loanapi.models.BlacklistModel;
import org.hegliens.loanapi.services.BlacklistService;

import com.google.gson.Gson;

@Path("blacklist")
public class BlacklistResource {
	BlacklistService service = new BlacklistService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response GetBlacklistEntries()
	{
		List<BlacklistModel> list = service.GetBlacklistEntries();
		return Response.status(Response.Status.OK).entity(new Gson().toJson(list)).build();
	}
	
	@Path("/{persId}")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public String AddBlacklistEntry(@PathParam("persId") String persId)
	{
		return service.AddBlacklistEntry(persId);
	}
	
	@Path("/{persId}")
	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	public String RemoveBlacklistEntry(@PathParam("persId") String persId)
	{
		return service.RemoveBlacklistEntry(persId);
	}
}
