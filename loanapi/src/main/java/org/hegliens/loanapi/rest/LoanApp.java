package org.hegliens.loanapi.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.hegliens.loanapi.resources.LoanResource;

@ApplicationPath("/webapi")
public class LoanApp extends ResourceConfig {
	public LoanApp()
	{
		register(LoanResource.class);
		//register(AuthorizationRequestFilter.class);
	}
}
