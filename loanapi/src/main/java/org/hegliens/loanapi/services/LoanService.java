package org.hegliens.loanapi.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.hegliens.loanapi.models.LoanModel;
import org.hegliens.loanapi.models.LoanRegisterModel;
import org.hegliens.loanapi.models.ProcessLoanResponseModel;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class LoanService {
	int maxCountryAppInSecond = 45;
	
	private DatabaseService dbService = new DatabaseService();
	
	public ProcessLoanResponseModel ProcessLoan(LoanModel loan,String ipAddress) throws IOException
	{
		ProcessLoanResponseModel response = new ProcessLoanResponseModel();
		response.setIpAddress(ipAddress);
		if(getDbService().IsEntryInBlacklist(loan.getPersId()))
			response.setResponse("ERROR: Person is in the blacklist!");
		else
		{
			loan.setOriginCountry(DetermineOriginCountry(ipAddress));
		    int appsInSecond = getDbService().GetCountryApplicationInLastSecond(loan.getOriginCountry());
			if(appsInSecond > maxCountryAppInSecond)
				response.setResponse("ERROR: Too much applications from country with " + loan.getOriginCountry() + " code!");
			else
			{
				getDbService().AddLoanRegisterEntry(loan.getOriginCountry());
				loan.setID(getDbService().AddApprovedLoan(loan));
				response.setLoan(loan);
				response.setResponse("SUCCESS: Loan is allowed!");
			}
		}
		return response;
	}
	
	String DetermineOriginCountry(String ipAddress)
	{
		//SAKH53HGE274RYP9KC8Z
		try {
			URL url = new URL("http://api.eurekapi.com/iplocation/v1.8/locateip?key=SAKH53HGE274RYP9KC8Z&ip=" + ipAddress + "&format=JSON");
		    HttpURLConnection request = (HttpURLConnection) url.openConnection();
		    request.connect();
		    JsonParser jp = new JsonParser();
		    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		    JsonObject rootobj = root.getAsJsonObject();
		    JsonObject statusObj = rootobj.getAsJsonObject("query_status");
		    String statusString = statusObj.get("query_status_code").getAsString();
		    if(!statusString.equals("OK"))
		    	return "LV";
		    else
		    {
		    	JsonObject locationObj = rootobj.getAsJsonObject("geolocation_data");
		    	return locationObj.get("country_code_iso3166alpha2").getAsString();
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "LV";
	}
	
	/*String DetermineOriginCountry(String ipAddress)
	{
		URL url = new URL("http://ip-api.com/json/" + ipAddress);
	    HttpURLConnection request = (HttpURLConnection) url.openConnection();
	    request.connect();
	    JsonParser jp = new JsonParser();
	    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
	    JsonObject rootobj = root.getAsJsonObject();
	    String statusString = rootobj.get("status").getAsString();
	    if(statusString.equals("fail"))
	    	return "LV";
	    else
	    	return rootobj.get("countryCode").getAsString();
	}
	
	String DetermineOriginCountry(String ipAddress)
	{
		return dbService.IpToCountryCode(ipAddress);
	}*/
	
	public List<LoanModel> GetApprovedLoans(String persId)
	{
		return getDbService().GetApprovedLoans(persId);
	}
	
	public List<LoanModel> GetApprovedLoans()
	{
		return getDbService().GetApprovedLoans();
	}
	
	public void ClearApprovedLoans()
	{
		getDbService().ClearApprovedLoans();
	}
	
	public List<LoanRegisterModel> GetLoanRegisterEntries()
	{
		return getDbService().GetLoanRegisterEntries();
	}

	public DatabaseService getDbService() {
		return dbService;
	}

	public void setDbService(DatabaseService dbService) {
		this.dbService = dbService;
	}
}
