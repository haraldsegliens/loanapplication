package org.hegliens.loanapi.services;

import java.util.List;

import org.hegliens.loanapi.models.BlacklistModel;

public class BlacklistService {
	DatabaseService dbService = new DatabaseService();
	public List<BlacklistModel> GetBlacklistEntries()
	{
		return dbService.GetBlacklistEntries();
	}
	
	public String AddBlacklistEntry(String persId)
	{
		dbService.AddBlacklistEntry(persId);
		return "Entry added!";
	}
	
	public String RemoveBlacklistEntry(String persId)
	{
		dbService.RemoveBlacklistEntry(persId);
		return "Entry removed!";
	}
}
