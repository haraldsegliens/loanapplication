package org.hegliens.loanapi.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.hegliens.loanapi.models.BlacklistModel;
import org.hegliens.loanapi.models.IpCountryModel;
import org.hegliens.loanapi.models.LoanModel;
import org.hegliens.loanapi.models.LoanRegisterModel;

public class DatabaseService {
	
	private Connection con = null;
	//ApprovedLoans (Id,LoanAmount,Term,Name,Surname,PersId,OriginCountry)
	//Blacklist (Id,PersId)
	//LoanRegister (Id,CountryCode,Created(DateTime))
	
	private HashMap<String,LoanRegisterModel> loanRegister = new HashMap<String,LoanRegisterModel>();
	private ArrayList<IpCountryModel> ipCountryList = new ArrayList<IpCountryModel>();
	public DatabaseService()
	{
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:loan.db");
			con.prepareStatement("create table if not exists ApprovedLoans (Id INTEGER PRIMARY KEY, LoanAmount REAL, Term TEXT, Name TEXT, Surname TEXT, PersId TEXT, OriginCountry TEXT)").execute();
			con.prepareStatement("create table if not exists Blacklist (Id INTEGER PRIMARY KEY, PersId TEXT)").execute();
			//con.prepareStatement("create table if not exists LoanRegister (Id INTEGER PRIMARY KEY, CountryCode TEXT, CreatedTime TEXT)").execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		getClass().getClassLoader().getResourceAsStream("../resources/dbip_country.csv");
		
	}
	
	public void AddApprovedLoan(LoanModel loan)
	{
		try {
			PreparedStatement addApprovedLoan = con.prepareStatement("insert or replace into ApprovedLoans values (NULL, ?, ?, ?, ?, ?, ?)");
			addApprovedLoan.setFloat(1, loan.getLoanAmount());
			addApprovedLoan.setString(2, loan.getTerm());
			addApprovedLoan.setString(3, loan.getName());
			addApprovedLoan.setString(4, loan.getSurname());
			addApprovedLoan.setString(5, loan.getPersId());
			addApprovedLoan.setString(6, loan.getOriginCountry());
			addApprovedLoan.execute();
			addApprovedLoan.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public List<LoanModel> GetApprovedLoans()
	{
		List<LoanModel> result = new ArrayList<LoanModel>();
		try {
			PreparedStatement getApprovedLoans = con.prepareStatement("select * from ApprovedLoans");
			ResultSet rs = getApprovedLoans.executeQuery();
			while(rs.next())
			{
				LoanModel newEntry = new LoanModel();
				newEntry.setID(rs.getInt("Id"));
				newEntry.setLoanAmount(rs.getFloat("LoanAmount"));
				newEntry.setName(rs.getString("Name"));
				newEntry.setSurname(rs.getString("Surname"));
				newEntry.setPersId(rs.getString("PersId"));
				newEntry.setOriginCountry(rs.getString("OriginCountry"));
				result.add(newEntry);
			}
			rs.close();
			getApprovedLoans.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public List<LoanModel> GetApprovedLoans(String persId)
	{
		List<LoanModel> result = new ArrayList<LoanModel>();
		try {
			PreparedStatement getApprovedLoansByUser = con.prepareStatement("select * from ApprovedLoans where persId = ?");
			getApprovedLoansByUser.setString(1, persId);
			ResultSet rs = getApprovedLoansByUser.executeQuery();
			while(rs.next())
			{
				LoanModel newEntry = new LoanModel();
				newEntry.setID(rs.getInt("Id"));
				newEntry.setLoanAmount(rs.getFloat("LoanAmount"));
				newEntry.setName(rs.getString("Name"));
				newEntry.setSurname(rs.getString("Surname"));
				newEntry.setPersId(rs.getString("PersId"));
				newEntry.setOriginCountry(rs.getString("OriginCountry"));
				result.add(newEntry);
			}
			rs.close();
			getApprovedLoansByUser.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public void ClearApprovedLoans()
	{
		try {
			PreparedStatement clearApprovedLoans = con.prepareStatement("delete from ApprovedLoans");
			clearApprovedLoans.executeUpdate();
			clearApprovedLoans.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void AddBlacklistEntry(String persId)
	{
		try {
			PreparedStatement addBlacklistEntry = con.prepareStatement("insert or replace into Blacklist values(NULL, ?)");
			addBlacklistEntry.setString(1,persId);
			addBlacklistEntry.execute();
			addBlacklistEntry.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<BlacklistModel> GetBlacklistEntries()
	{
		List<BlacklistModel> result = new ArrayList<BlacklistModel>();
		try {
			PreparedStatement getBlacklistEntries = con.prepareStatement("select * from Blacklist");
			ResultSet rs = getBlacklistEntries.executeQuery();
			while(rs.next())
			{
				BlacklistModel newEntry = new BlacklistModel();
				newEntry.setID(rs.getInt("ID"));
				newEntry.setPersId(rs.getString("persId"));
				result.add(newEntry);
			}
			rs.close();
			getBlacklistEntries.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean IsEntryInBlacklist(String persId)
	{		
		try {
			PreparedStatement countBlacklistEntry = con.prepareStatement("select count(*) as count from Blacklist where persId = ?");
			countBlacklistEntry.setString(1,persId);
			ResultSet rs = countBlacklistEntry.executeQuery();
			int count = rs.getInt("count");
			rs.close();
			countBlacklistEntry.close();
			return count != 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public void RemoveBlacklistEntry(String persId)
	{
		try {
			PreparedStatement deleteBlacklistEntry = con.prepareStatement("delete from Blacklist where persId = ?");
			deleteBlacklistEntry.setString(1, persId);
			deleteBlacklistEntry.execute();
			deleteBlacklistEntry.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void AddLoanRegisterEntry(String countryCode)
	{
		int currentTime = (int) (Calendar.getInstance().getTimeInMillis()/1000);
		
		LoanRegisterModel entry;
		if(loanRegister.containsKey(countryCode))
			entry = loanRegister.get(countryCode);
		else
		{
			entry = new LoanRegisterModel();
			entry.setCountryCode(countryCode);
			entry.setCreatedTime(currentTime);
			entry.setCalls(0);
			loanRegister.put(countryCode, entry);
		}
		if(currentTime == entry.getCreatedTime())//compare seconds
		{
			entry.setCalls(entry.getCalls() + 1);
		}
		else
		{
			entry.setCreatedTime(currentTime);
			entry.setCalls(1);
		}	
	}
	
	public List<LoanRegisterModel> GetLoanRegisterEntries()
	{
		return new ArrayList<LoanRegisterModel>(loanRegister.values());
	}
	
	public int GetCountryApplicationInLastSecond(String countryCode)
	{
		int currentTime = (int) (Calendar.getInstance().getTimeInMillis()/1000);
		if(!loanRegister.containsKey(countryCode)) 
			return 0;
		LoanRegisterModel entry = loanRegister.get(countryCode);
		if(currentTime != entry.getCreatedTime()) 
			return 0;
		return entry.getCalls();
	}
	
	public String IpToCountryCode(String ip)
	{
		long ip_long = IpCountryModel.ConvertIpAddressToLong(ip);
		int 
		ipCountryList
		return "";
	}
	
	int BS_ipCountryList(long target_ip,long startIndex,long endIndex)
	{
		long middleIndex = (startIndex+endIndex)/2;
		if(ipCountryList.get(middleIndex))
	}
	
}
