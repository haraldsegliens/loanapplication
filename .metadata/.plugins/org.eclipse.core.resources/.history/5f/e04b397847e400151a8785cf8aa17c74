package org.hegliens.loanapi.resources;

import java.io.IOException;
import java.util.List;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hegliens.loanapi.models.LoanModel;
import org.hegliens.loanapi.models.LoanRegisterModel;
import org.hegliens.loanapi.services.LoanService;

import com.google.gson.Gson;

@Path("loans") @Singleton
public class LoanResource {
	LoanService service = new LoanService();
	//ISO8859_13
	@Context
    HttpServletRequest requestContext;
	
	@POST @Consumes(MediaType.APPLICATION_JSON) @Produces(MediaType.TEXT_PLAIN)
	public Response ApplyLoan(LoanModel loan) throws IOException
	{
		String loanStatusString = service.ProcessLoan(loan, requestContext.getRemoteAddr());
		if(loanStatusString.contains("SUCCESS"))
			return Response.status(Response.Status.CREATED).entity(loanStatusString).build(); 
		else
			return Response.status(500).entity(loanStatusString).build(); 
	}
	@GET @Produces(MediaType.APPLICATION_JSON)
	public Response GetAllowedLoans()
	{
		List<LoanModel> list = service.GetApprovedLoans();
		return Response.status(Response.Status.OK).entity(new Gson().toJson(list)).build();
	}
	
	@Path("{persId}") @GET @Produces(MediaType.APPLICATION_JSON)
	public Response GetAllowedLoansByUser(@PathParam("persId") String personId)
	{
		List<LoanModel> list = service.GetApprovedLoans(personId);
		return Response.status(Response.Status.OK).entity(new Gson().toJson(list)).build();
	}

	@DELETE @Produces(MediaType.TEXT_PLAIN)
	public String ClearAllowedLoans()
	{
		service.ClearApprovedLoans();
		return "Approved loans cleared!";
	}
	
	@GET @Path("register") @Produces(MediaType.APPLICATION_JSON)
	public Response GetLoanRegisterEntries()
	{
		List<LoanRegisterModel> list = service.GetLoanRegisterEntries();
		return Response.status(Response.Status.OK).entity(new Gson().toJson(list)).build();
	}
}
